# dfm
dfm is a simple, configurable, and fast backup manager (originally for dotfiles).


### Features
- `git` is optional.
- retains file structure.
- symlinks are copied as the linked file.
- creates a blank config *(`~/.config/dfm/dfmrc`)* template.

### Requirements
- rsync
- git *(optional)*

### Simple example config

```
#!/bin/bash
# shellcheck disable=SC2034

# name for the directory to use/create in BPATH.
BDIR=configs

# location for backup/dotfile folder or repo to be created/cloned
BPATH=/media/wdblue/git/personal/dotfiles

# git repo url for cloning/pushing (leave empty for local backup). If this
# is set to a non-existant url or a url that isn't a git repo, expect errors
REPO='https://bitbucket.org/natemaia/dotfiles'

# git branch used when pushing changes, defaults to HEAD
BRANCH=""

# file paths to back up as BPATH/BDIR/BKPS
# qoutes surround paths that contain spaces or expect errors
BKPS=(
	~/bin
	~/.zsh
	~/.mpd
	~/.xinitrc
	~/.zprofile
	~/.gitconfig
	~/.gitignore
	~/.terminfo
	~/.local/bin
	~/.config/dk
	~/.config/dfm
	~/.config/dunst
	~/.config/sxhkd
	~/.config/unity3d
	~/.config/picom.conf
	~/.config/fontconfig
	~/.config/qutebrowser
	~/.config/nvim/after
	~/.config/nvim/spell
	~/.config/nvim/init.vim
	~/.config/mimeapps.list
	~/.gnupg/gpg-agent.conf
	/etc/X11/xorg.conf.d
	/etc/modprobe.d
)
```

Because it's sourced as a bash script, this allows command substitution,
brace expansion, additional scripting, or anything else that works.

```
#!/bin/bash
# shellcheck disable=SC2034

BDIR=configs
BPATH=/media/wdblue/git/personal/dotfiles
REPO='https://bitbucket.org/natemaia/dotfiles'

BKPS=(
	~/{bin,.zsh,.zshrc,.zprofile,.xinitrc,.dfmrc}
	~/.{inputrc,gnupg/gpg-agent.conf,gitconfig}
	~/.config/nvim/{init.vim,after,spell,plugin}
	~/.config/{dunst,fontconfig,mimeapps.list}
)
```

### Usage
If `~/.config/dfm/dfmrc` does not exist one will be created, this needs to be edited
before dfm will actually do anything.

```
USAGE:  dfm [OPTIONS]

OPTIONS:
        -h         Display this usage message
        -b         Backup files listed in ~/.config/dfm/dfmrc
        -r         Restore a previous backup
        -c         Clean existing backup
        -p         Push changes to git origin

 Configuration is done in ~/.config/dfm/dfmrc
 A default will be created if it doesn't exist.

 Without any options a selection menu is opened
```
