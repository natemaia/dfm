#!/bin/bash

# a simple, configurable, and fast backup manager (originally for dotfiles).
#
# written by Nathaniel Maia <natemaia10@gmail.com> -- 2018-2021


typeset -gr NAME="dfm"
typeset -gr CFG="${NAME}rc"
typeset -gr CFGDIR="${XDG_CONFIG_HOME:-$HOME/.config}/$NAME"
typeset -i BACKUP RESTORE PUSH # ints

PS3=$'\nEnter selection (0/q to exit): '

main()
{
	prep_dir || exit 1
	if (( BACKUP )); then
		backup && printf "\nBackup complete\n"
	elif (( RESTORE )); then
		restore && printf "\nRestore complete\n"
	elif (( PUSH )); then
		git_commit && printf "\nGit commit complete\n"
	fi
}

menu()
{
	printf "\e[H\e[2J\n"
	opts=("Backup" "Restore" "Clean and backup" "Clean, backup, and push")
	if (( ${#REPO} )); then
		select OPT in "${opts[@]}"; do break; done
	else
		select OPT in "${opts[0]}" "${opts[1]}" "${opts[2]}"; do break; done
	fi
	clear
	case "$OPT" in
		"${opts[0]}") BACKUP=1 ;;
		"${opts[1]}") RESTORE=1 ;;
		"${opts[2]}") BACKUP=1; clean_dir ;;
		"${opts[3]}") BACKUP=1 PUSH=1; clean_dir ;;
		*) exit 0 ;;
	esac
}

usage()
{
	cat <<EOF
USAGE:  $NAME [OPTIONS]

OPTIONS:
        -h         Display this usage message
        -b         Backup files listed in $CFGDIR/$CFG
        -r         Restore a previous backup
        -c         Clean existing backup
        -p         Push changes to git origin

 Configuration is done in $CFGDIR/$CFG
 A default will be created if it doesn't exist.

 Without any options a selection menu is opened
EOF
	exit 0
}

backup()
{
	local to="" from=""

	(( ${#BKPS[@]} )) || { printf "no paths to backup..\nconfiguration is done in ~/.%src\n" "$NAME"; return 1; }

	if read -re -p "Create a unique date-stamped snapshot directory for this backup? [y/N]: " ans && [[ ${ans,,} == 'y' ]]; then
		to="$BPATH/$(date +%F-%H)"
	else
		[[ $BDIR == *'../'* ]] && { echo "error: using ../ in BDIR will put you out of bounds.."; return 1; }
		to="$BPATH/$BDIR"
	fi

	mkdir -pv "$to"
	for i in "${BKPS[@]}"; do
		[[ -e "$i" ]] && rsync -aRvhb --suffix=".bak" --safe-links --progress "$i" "$to"
	done

	{ (( ! PUSH )) && return; } || git_commit
}

mk_cfg()
{
	mkdir -p "$CFGDIR" || return
	cat > "$CFGDIR/$CFG" << EOF
# dfm config file
# sourced as a bash script, this allows command substitution,
# brace expansion, additional scripting, and anything else that works.

# git repo url for cloning/pushing (leave empty for local backup). If this
# is set to a non-existant url or a url that isn't a git repo, expect errors
REPO=""

# git branch used when pushing changes, defaults to HEAD
BRANCH=""

# location for backup/dotfile folder or repo to be created/cloned
BPATH=""

# name for the directory to use/create in BPATH.
BDIR="configs"

# file paths to back up as BPATH/BDIR/BKPS
# qoutes surround paths that contain spaces or expect errors
BKPS=()

# vim:ft=sh
EOF
	printf "New config created: %s\n\nIt needs to be configured before %s will do anything\n" "$CFGDIR/$CFG" "$NAME"
	exit 0
}

restore()
{
	[[ -d $BPATH/$BDIR ]] || { echo "error: missing directory: $BPATH/$BDIR"; return 1; }
	local to='' from='' ans=''

	printf "\e[H\e[2J\n"
	if read -re -p "Use a date-stamped snapshot directory for this restore? [y/N]: " ans && [[ ${ans,,} == 'y' ]]; then
		printf "\e[H\e[2J\n"
		select OPT in $(command ls | grep -o "[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}-[0-9]\{2\}"); do break; done
		{ [[ $OPT && -d $BPATH/$OPT ]] && BDIR=$OPT; } || return 1
	fi

	printf "\nThis will overwrite the following files with the backups stored in %s\n\n" "$BPATH/$BDIR"
	printf "\t%s\n" "${BKPS[@]}"
	read -re -p $'\nDo you want to continue? [y/N]: ' ans
	[[ ${ans,,} == 'y' ]] || return 1

	for i in "${BKPS[@]}"; do
		to="$i"
		from="$BPATH/${BDIR}${i}"
		[[ -e $from ]] || continue
		if [[ -d $from ]]; then
			to+='/'
			from+='/.'
		fi
		if [[ $i == /home/* ]]; then
			rsync -avhb --suffix=".bak" --safe-links --progress "$from" "$to"
		else
			sudo rsync -avhb --suffix=".bak" --safe-links --progress "$from" "$to"
		fi
	done
}

prep_dir()
{
    [[ $BPATH ]] || { echo "BPATH must be set in config."; return 1; }
    [[ $REPO ]] || { mkdir -p "$BPATH"; return 0; }
    if [[ -d $BPATH/.git ]]; then
		cd "$BPATH" && git pull && return 0
	else
		rm -rf "$BPATH" >/dev/null 2>&1; git clone "$REPO" "$BPATH"
	fi
}

clean_dir()
{
    [[ -d $BPATH/$BDIR ]] && rm -rf "$BPATH/${BDIR:?}"
}

git_commit()
{
	[[ $REPO ]] && cd "$BPATH/" || return 1
	git add .
	read -re -p $'\nEnter a short summary of this commit.\n\n> ' msg
	git commit -m "${msg:-$(date +%a-%D) update}"
	git push origin "${BRANCH:-HEAD}"
}

# shellcheck disable=1090
. "$CFGDIR/$CFG" || mk_cfg

if ! hash rsync >/dev/null 2>&1; then
	echo "error: this requires rsync installed"; exit 1
elif (( ${#REPO} )) && ! hash git >/dev/null 2>&1; then
	echo "error: using REPO in your ${NAME}rc is unsupported without git, install it first"; exit 1
fi

if (( $# == 0 )); then
	menu
else
	while getopts ":hbrcp" OPT; do
		case "$OPT" in
			h) usage ;;
			p) PUSH=1 ;;
			b) BACKUP=1 ;;
			r) RESTORE=1 ;;
			c) clean_dir ;;
			\?) echo "error: invalid option: -$OPTARG"; exit 1 ;;
		esac
	done
fi

(( BACKUP || RESTORE || PUSH )) && main

# vim:ft=sh:fdm=marker:fmr={,}
